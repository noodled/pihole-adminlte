## Original Pi-hole AdminLTE theme
[https://github.com/pi-hole/AdminLTE](https://github.com/pi-hole/AdminLTE)
## Dark theme installer
[https://github.com/lkd70/PiHole-Dark](https://github.com/lkd70/PiHole-Dark)
## Customizations
### Background image
Copy file located in `repo-dir/var/www/html/admin/img/boxed-bg.jpg` to `/var/www/html/admin/img/boxed-bg.jpg`
### Dark "this site has been blocked" page
Copy files located in `repo-dir/var/www/html/pihole/blockingpage.css` and `repo-dir/var/www/html/pihole/index.html` to folder `/var/www/html/pihole/`
### Migrate to font-awesome 5.0.13, add status page & shit
- Copy the folder `repo-dir/var/www/html/admin/style/vendor/font-awesome-5.0.13` to `/var/www/html/admin/style/vendor/`
- Copy the file `repo-dir/var/www/html/admin/status.php` to `/var/www/html/admin/status.php`
- Copy the file `repo-dir/var/www/html/admin/scripts/pi-hole/php/header.php` to `/var/www/html/admin/scripts/pi-hole/php/header.php`
- Delete folder `/var/www/html/admin/style/vendor/font-awesome-4.5.0`